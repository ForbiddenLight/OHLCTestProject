package com.bumbyk.alexander.ohlcchart;

interface Presenter {
    void getTickers();

    void getChart(String ticker, String dataProviderName);

    void onDestroy();
}
