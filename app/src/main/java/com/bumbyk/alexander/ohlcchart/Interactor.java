package com.bumbyk.alexander.ohlcchart;

import com.github.mikephil.charting.data.CandleEntry;

import java.util.ArrayList;

interface Interactor {

    interface OnDownloadFinishedListener{

        void onTickersDownloadFinished(ArrayList<String> tickers);

        void onChartDownloadFinished(ArrayList<CandleEntry> entries,  ArrayList<String> quarters);
    }

    void downloadTickers(OnDownloadFinishedListener listener);

    void downloadChart(String ticker, String dataProviderName, OnDownloadFinishedListener listener);
}
