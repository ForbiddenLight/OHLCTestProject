package com.bumbyk.alexander.ohlcchart;

import android.graphics.Color;
import android.graphics.Paint;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.data.CandleData;
import com.github.mikephil.charting.data.CandleDataSet;
import com.github.mikephil.charting.data.CandleEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.util.ArrayList;

class PresenterImpl implements Presenter, Interactor.OnDownloadFinishedListener {
    private MainView mainView;
    private Interactor interactor;

    PresenterImpl(MainView mainView) {
        this.mainView = mainView;
        this.interactor = new InteractorImpl();
    }

    @Override
    public void getTickers() {
        if (mainView.hasNetworkConnection()) {
            mainView.showLoading();
            interactor.downloadTickers(this);
            mainView.setOnItemsClickListeners();
        } else {
            mainView.showConnectionErrorMessage();
        }
    }

    @Override
    public void getChart(String ticker, String dataProviderName) {
        if (mainView.hasNetworkConnection()) {
            mainView.showProgress();
            interactor.downloadChart(ticker, dataProviderName, this);
        } else {
            mainView.showConnectionErrorMessage();
        }
    }

    @Override
    public void onDestroy() {
        mainView = null;
    }

    @Override
    public void onTickersDownloadFinished(ArrayList<String> tickers) {
        if (mainView != null) {
            if (tickers.isEmpty()) {
                mainView.showConnectionErrorMessage();
            } else {
                mainView.hideLoading();
                mainView.setTextViewAdapter(tickers);
            }
        }
    }

    @Override
    public void onChartDownloadFinished(ArrayList<CandleEntry> entries, final ArrayList<String> quarters) {
        if (mainView != null) {
            if (entries.isEmpty() || quarters.isEmpty()) {
                mainView.showConnectionErrorMessage();
            } else {
                CandleData data = null;
                IAxisValueFormatter formatter = null;
                try {
                    mainView.hideProgress();

                    String ticker = mainView.getTextFromTextView();

                    CandleDataSet dataSet = new CandleDataSet(entries, ticker);
                    dataSet.setDrawHorizontalHighlightIndicator(true);
                    dataSet.setDrawVerticalHighlightIndicator(true);

                    dataSet.setShadowColor(Color.DKGRAY);
                    float dataSetShadowWidth = 2.0f;
                    dataSet.setShadowWidth(dataSetShadowWidth);

                    dataSet.setDecreasingColor(Color.GREEN);
                    dataSet.setDecreasingPaintStyle(Paint.Style.STROKE);

                    dataSet.setIncreasingColor(Color.RED);
                    dataSet.setIncreasingPaintStyle(Paint.Style.FILL);

                    dataSet.setNeutralColor(Color.BLUE);

                    data = new CandleData(dataSet);
                    data.setDrawValues(true);
                    data.setHighlightEnabled(true);

                    formatter = new IAxisValueFormatter() {
                        @Override
                        public String getFormattedValue(float value, AxisBase axis) {
                            try {
                                int intValue = (int) value;
                                //to correct quarters drawing
                                if (intValue != 0) {
                                    intValue -= 1;
                                }
                                return quarters.get(intValue);
                            } catch (Exception e) {
                                //default label
                                return "XX-XX-XX";
                            }
                        }
                    };
                } catch (Exception ignore) {
                }
                mainView.drawCandleStickChart(data, formatter);
            }
        }
    }
}