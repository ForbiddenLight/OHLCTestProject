package com.bumbyk.alexander.ohlcchart;

import java.util.HashMap;

final class URLKeeper {
    final static String tickersURL = "https://s3.amazonaws.com/quandl-static-content/Ticker+CSV%27s/WIKI_tickers.csv";
    final static String fileType = ".csv";
    final static String suffix = "WIKI/";
    static HashMap<String, String> providersMap = new HashMap<>();

    static {
        providersMap.put("Quandl.com", "https://www.quandl.com/api/v3/datasets/WIKI/");
        providersMap.put("Google.com", "https://www.google.com/finance/historical?output=csv&q=");
        providersMap.put("Yahoo.com", "https://ichart.finance.yahoo.com/table.csv?d=6&e=1&g=d&a=7&b=19&ignore=.csv&s=");
        providersMap.put("Quotemedia.com", "https://app.quotemedia.com/quotetools/getHistoryDownload.csv?&webmasterId=501&isRanged=false&symbol=");
    }

    private URLKeeper(){

    }
}
