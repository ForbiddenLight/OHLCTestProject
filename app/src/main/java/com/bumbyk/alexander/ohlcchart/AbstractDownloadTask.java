package com.bumbyk.alexander.ohlcchart;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

abstract class AbstractDownloadTask extends AsyncTask<String, Void, Void> {
    private InteractorImpl interactor;

    AbstractDownloadTask(InteractorImpl interactor){
        this.interactor = interactor;
    }

    @Override
    protected Void doInBackground(String... params) {
        try {
            URL url = new URL(params[0]);
            HttpsURLConnection c = (HttpsURLConnection) url.openConnection();
            c.connect();
            interactor.setReader(new BufferedReader(new InputStreamReader(c.getInputStream())));
            read();
            interactor.getReader().close();
        } catch (IOException ignore) {

        }
        return null;
    }

    abstract void read();
}