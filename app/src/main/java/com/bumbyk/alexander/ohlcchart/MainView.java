package com.bumbyk.alexander.ohlcchart;

import com.github.mikephil.charting.data.CandleData;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.util.ArrayList;

interface MainView {
    void setOnItemsClickListeners();

    void setTextViewAdapter(ArrayList<String> tickers);

    String getTextFromTextView();

    void drawCandleStickChart(CandleData data, IAxisValueFormatter formatter);

    void showProgress();

    void hideProgress();

    boolean hasNetworkConnection();

    void showConnectionErrorMessage();

    void showLoading();

    void hideLoading();
}
