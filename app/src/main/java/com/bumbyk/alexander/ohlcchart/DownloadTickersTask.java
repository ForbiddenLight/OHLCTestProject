package com.bumbyk.alexander.ohlcchart;

import java.io.IOException;
import java.util.ArrayList;

class DownloadTickersTask extends AbstractDownloadTask {
    private Interactor.OnDownloadFinishedListener listener;
    private InteractorImpl interactor;

    DownloadTickersTask(Interactor.OnDownloadFinishedListener listener, InteractorImpl interactor){
        super(interactor);
        this.listener = listener;
        this.interactor = interactor;
    }

    @Override
    protected void read() {
        try {
            interactor.setTickers(new ArrayList<String>());
            String line;
            while ((line = interactor.getReader().readLine()) != null) {
                if (line.startsWith(URLKeeper.suffix)){
                    line = line.replaceAll("\"", " ");
                    line = line.substring(URLKeeper.suffix.length());
                    interactor.getTickers().add(line);
                }
            }
        } catch (IOException e) {
            //clear data in case of error
            interactor.setTickers(new ArrayList<String>());
        }
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        listener.onTickersDownloadFinished(interactor.getTickers());
    }
}
