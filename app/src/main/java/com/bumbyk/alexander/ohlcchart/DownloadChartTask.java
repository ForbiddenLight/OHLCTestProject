package com.bumbyk.alexander.ohlcchart;

import com.github.mikephil.charting.data.CandleEntry;

import java.io.IOException;
import java.util.ArrayList;

class DownloadChartTask extends AbstractDownloadTask {
    private Interactor.OnDownloadFinishedListener listener;
    private InteractorImpl interactor;

    DownloadChartTask(Interactor.OnDownloadFinishedListener listener, InteractorImpl interactor) {
        super(interactor);
        this.listener = listener;
        this.interactor = interactor;
    }

    @Override
    protected void read() {
        try {
            interactor.setCandleEntries(new ArrayList<CandleEntry>());
            interactor.setQuarters(new ArrayList<String>());
            int entriesCount = 0;
            String line;
            while ((line = interactor.getReader().readLine()) != null) {
                try {
                    if (entriesCount > 0) {
                        String[] data = line.split(",");

                        interactor.getQuarters().add(data[0]);
                        float open = Float.parseFloat(data[1]);
                        float high = Float.parseFloat(data[2]);
                        float low = Float.parseFloat(data[3]);
                        float close = Float.parseFloat(data[4]);

                        CandleEntry entry = new CandleEntry((float) entriesCount, high, low, open, close);
                        interactor.getCandleEntries().add(entry);
                    }
                    entriesCount++;
                } catch (Exception ignore) {
                    //ignore invalid lines
                }
            }
        } catch (IOException e) {
            //clear data in case of error
            interactor.setCandleEntries(new ArrayList<CandleEntry>());
            interactor.setQuarters(new ArrayList<String>());
        }
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        listener.onChartDownloadFinished(interactor.getCandleEntries(), interactor.getQuarters());
    }
}
