package com.bumbyk.alexander.ohlcchart;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.github.mikephil.charting.charts.CandleStickChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.CandleData;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements MainView {
    private AutoCompleteTextView autoCompleteTextView;
    private CandleStickChart candleStickChart;
    private ProgressBar progressBar;
    private TextView textView;
    private Button button;
    private Spinner spinner;
    private RelativeLayout relativeLayout;
    private Presenter presenter;
    private boolean isTickersLoaded = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        prepareComponents();

        presenter = new PresenterImpl(this);
        presenter.getTickers();
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void setOnItemsClickListeners() {
        autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String viewText = autoCompleteTextView.getText().toString();

                //ticker - is abbreviation of company name
                String ticker = viewText.split(",")[0];
                autoCompleteTextView.setText(ticker);

                int cursorPosition = ticker.length();
                autoCompleteTextView.setSelection(cursorPosition);

                String dataProviderName = spinner.getSelectedItem().toString();
                presenter.getChart(ticker, dataProviderName);

                closeKeyboard(view);
            }
        });

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                String ticker = autoCompleteTextView.getText().toString();
                if (!ticker.isEmpty()) {
                    if (!candleStickChart.isEmpty()) {

                        //checking that ticker wasn't changed and it's valid
                        //chartTicker contains ticker from last downloaded chart
                        //so we can download data from another data provider but with SAME ticker
                        String chartTicker = candleStickChart.getCandleData().getDataSetByIndex(0).getLabel();
                        if (ticker.equals(chartTicker)) {
                            String dataProviderName = spinner.getSelectedItem().toString();
                            presenter.getChart(ticker, dataProviderName);
                        }
                    }
                }
                closeKeyboard(selectedItemView);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                //do nothing
            }
        });
    }

    @Override
    public void setTextViewAdapter(ArrayList<String> tickers) {
        TickerArrayAdapter adapter = new TickerArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, tickers);
        autoCompleteTextView.setAdapter(adapter);
        isTickersLoaded = true;
    }

    @Override
    public void drawCandleStickChart(CandleData data, IAxisValueFormatter formatter) {
        try {
            candleStickChart.setData(data);
            candleStickChart.invalidate();

            Description description = new Description();
            String chartType = getResources().getString(R.string.chartType);
            description.setText(chartType);
            candleStickChart.setDescription(description);
            candleStickChart.setDrawBorders(true);

            XAxis xAxis = candleStickChart.getXAxis();
            float xAxisGranularity = 0.5f;
            xAxis.setGranularity(xAxisGranularity);
            xAxis.setValueFormatter(formatter);
            float visibleXRangeMax = 16.0f;
            candleStickChart.setVisibleXRangeMaximum(visibleXRangeMax);

            int durationMillis = 500;
            candleStickChart.animateY(durationMillis);
            candleStickChart.animateX(durationMillis);
        } catch (Exception ignore) {
        }
    }

    @Override
    public String getTextFromTextView() {
        return autoCompleteTextView.getText().toString();
    }

    @Override
    public void showProgress() {
        candleStickChart.setVisibility(CandleStickChart.GONE);
        relativeLayout.setVisibility(RelativeLayout.GONE);
        progressBar.setVisibility(ProgressBar.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(ProgressBar.GONE);
        candleStickChart.setVisibility(CandleStickChart.VISIBLE);
    }

    @Override
    public void showLoading() {
        relativeLayout.setVisibility(RelativeLayout.GONE);
        spinner.setVisibility(Spinner.GONE);
        autoCompleteTextView.setVisibility(AutoCompleteTextView.GONE);
        candleStickChart.setVisibility(CandleStickChart.GONE);
        progressBar.setVisibility(ProgressBar.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(ProgressBar.GONE);
        spinner.setVisibility(Spinner.VISIBLE);
        autoCompleteTextView.setVisibility(AutoCompleteTextView.VISIBLE);
        autoCompleteTextView.requestFocus();
        candleStickChart.setVisibility(CandleStickChart.VISIBLE);
    }

    @Override
    public boolean hasNetworkConnection() {
        try {
            ConnectivityManager cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            return activeNetwork.isConnectedOrConnecting();
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void showConnectionErrorMessage() {
        progressBar.setVisibility(ProgressBar.GONE);
        candleStickChart.setVisibility(CandleStickChart.GONE);
        relativeLayout.setVisibility(RelativeLayout.VISIBLE);
        textView.setVisibility(TextView.VISIBLE);
        button.setVisibility(Button.VISIBLE);
    }

    public void onButtonClick(View view) {
        if (isTickersLoaded) {
            String companyName = autoCompleteTextView.getText().toString();
            String dataProvider = spinner.getSelectedItem().toString();
            presenter.getChart(companyName, dataProvider);
        } else {
            presenter.getTickers();
        }
    }

    public void closeKeyboard(View view) {
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
    }

    public void prepareComponents() {
        spinner = (Spinner) findViewById(R.id.spinner);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        candleStickChart = (CandleStickChart) findViewById(R.id.chart);
        autoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView);
        relativeLayout = (RelativeLayout) findViewById(R.id.relativeLayout);
        textView = (TextView) findViewById(R.id.networkErrorTextView);
        button = (Button) findViewById(R.id.retryConnectButton);

        spinner.setVisibility(Spinner.GONE);
        autoCompleteTextView.setVisibility(AutoCompleteTextView.GONE);

        String messageWhileNoData = getResources().getString(R.string.no_chart_data);
        candleStickChart.setNoDataText(messageWhileNoData);
    }
}