package com.bumbyk.alexander.ohlcchart;

import com.github.mikephil.charting.data.CandleEntry;

import java.io.BufferedReader;
import java.util.ArrayList;

class InteractorImpl implements Interactor {
    private ArrayList<CandleEntry> candleEntries = new ArrayList<>();
    private ArrayList<String> tickers = new ArrayList<>();
    private ArrayList<String> quarters = new ArrayList<>();
    private BufferedReader reader;

    @Override
    public void downloadTickers(OnDownloadFinishedListener listener) {
        new DownloadTickersTask(listener, this).execute(URLKeeper.tickersURL);
    }

    @Override
    public void downloadChart(String ticker, String dataProviderName, OnDownloadFinishedListener listener) {
        String dataProviderURL = URLKeeper.providersMap.get(dataProviderName);
        String fileType = URLKeeper.fileType;
        String URL;

        //because we have 2 types of providers URL
        if (dataProviderURL.endsWith(URLKeeper.suffix)) {
            URL = dataProviderURL + ticker + fileType;
        } else {
            URL = dataProviderURL + ticker;
        }
        new DownloadChartTask(listener, this).execute(URL);
    }

    public ArrayList<CandleEntry> getCandleEntries() {
        return candleEntries;
    }

    public void setCandleEntries(ArrayList<CandleEntry> candleEntries) {
        this.candleEntries = candleEntries;
    }

    public ArrayList<String> getTickers() {
        return tickers;
    }

    public void setTickers(ArrayList<String> tickers) {
        this.tickers = tickers;
    }

    public ArrayList<String> getQuarters() {
        return quarters;
    }

    public void setQuarters(ArrayList<String> quarters) {
        this.quarters = quarters;
    }

    public BufferedReader getReader() {
        return reader;
    }

    public void setReader(BufferedReader reader) {
        this.reader = reader;
    }
}
